#include <stdlib.h>
#include <gtk/gtk.h>

#include <epoxy/gl.h>
#include <gthree/gthree.h>
#include <gthree/gthreearea.h>

static GthreeAnimationMixer *mixer;
static GthreeGroup *fish;
static GthreeMesh *shiny;
static GthreeAnimationClip *clip;
static GthreeLoader *fish_loader = NULL;
static graphene_vec3_t shiny_velocity;

#define AQUARIUM_SIZE_X 800
#define AQUARIUM_SIZE_Y 600
#define AQUARIUM_SIZE_Z 400
#define SHINY_SPEED 6
#define FISH_SPEED 3
#define FISH_SCALE 40

//#define SHOW_SHINY

GthreeLoader *
load_model (const char *name)
{
  GError *error = NULL;
  g_autofree char *file = NULL;
  g_autoptr(GBytes) bytes = NULL;
  g_autoptr(GthreeLoader) loader = NULL;

  file = g_build_filename ("/org/gnome/wanda/models/", name, NULL);
  bytes = g_resources_lookup_data (file, G_RESOURCE_LOOKUP_FLAGS_NONE, NULL);
  if (bytes == NULL)
    g_error ("Failed to load model %s\n", name);

  loader = gthree_loader_parse_gltf (bytes, NULL, &error);
  if (loader == NULL)
    g_error ("Failed to %s: %s\n", name, error->message);

  return g_steal_pointer (&loader);
}

static void
resize_area (GthreeArea *area,
             gint width,
             gint height,
             GthreePerspectiveCamera *camera)
{
  gthree_perspective_camera_set_aspect (camera, (float)width / (float)(height));
}

static void
init_scene (GthreeScene *scene)
{
  GthreeAmbientLight *ambient_light;
  GthreeDirectionalLight *sun;
  graphene_vec3_t red, white, grey;

  graphene_vec3_init (&white, 1, 1, 1);
  graphene_vec3_init (&red, 1, 0, 0);
  graphene_vec3_init (&grey, 0.75, 0.75, 0.75);

  ambient_light = gthree_ambient_light_new (&grey);
  gthree_light_set_intensity (GTHREE_LIGHT (ambient_light), 0.7);

  gthree_object_add_child (GTHREE_OBJECT (scene), GTHREE_OBJECT (ambient_light));

  sun = gthree_directional_light_new (&white, 1.5);
  gthree_object_set_position_xyz (GTHREE_OBJECT (sun), -4000, 1200, 1800);
  gthree_object_look_at_xyz (GTHREE_OBJECT (sun), 0, 0, 0);
  gthree_object_add_child (GTHREE_OBJECT (scene), GTHREE_OBJECT (sun));

  fish_loader = load_model ("wanda.glb");

  fish = gthree_group_new ();
  gthree_object_add_child (GTHREE_OBJECT (scene), GTHREE_OBJECT (fish));

  GthreeScene *fish_scene = gthree_loader_get_scene (fish_loader, 0);
  gthree_object_add_child (GTHREE_OBJECT (fish), GTHREE_OBJECT (fish_scene));
  gthree_object_set_scale_uniform (GTHREE_OBJECT (fish), FISH_SCALE);
  gthree_object_set_position_xyz (GTHREE_OBJECT (fish), AQUARIUM_SIZE_X*0.7, 0, 0);

  clip = gthree_loader_get_animation (fish_loader, 0);
  if (clip != NULL)
    {
      GthreeAnimationAction *action = gthree_animation_mixer_clip_action (mixer, clip, NULL);

      gthree_animation_action_set_loop_mode (action, GTHREE_LOOP_MODE_REPEAT, -1);
      gthree_animation_action_set_enabled (action, TRUE);

      gthree_animation_action_play (action);
    }

  g_autoptr(GthreeGeometry) ball = gthree_geometry_new_sphere (20, 10, 10);
  g_autoptr(GthreeMeshBasicMaterial) ball_material = gthree_mesh_basic_material_new ();
  gthree_mesh_basic_material_set_color (ball_material, &red);

  shiny = gthree_mesh_new (ball, GTHREE_MATERIAL (ball_material));
  gthree_object_add_child (GTHREE_OBJECT (scene), GTHREE_OBJECT (shiny));

#ifndef SHOW_SHINY
  gthree_object_set_visible (GTHREE_OBJECT (shiny), FALSE);
#endif
  gthree_object_set_position_xyz (GTHREE_OBJECT (shiny), 40, 0, 0);

}

static gboolean
tick (GtkWidget     *widget,
      GdkFrameClock *frame_clock,
      gpointer       user_data)
{
  static gint64 last_frame_time_i = 0;
  gint64 frame_time_i;
  graphene_vec3_t fish_pos, shiny_pos, mv, dp;
  float mx = 1, my = 1, mz = 1;

  frame_time_i = gdk_frame_clock_get_frame_time (frame_clock);
  if (last_frame_time_i != 0)
    {
      float delta_time_sec = (frame_time_i - last_frame_time_i) / (float) G_USEC_PER_SEC;
      gthree_animation_mixer_update (mixer, delta_time_sec);
    }
  last_frame_time_i = frame_time_i;

  graphene_vec3_add (gthree_object_get_position (GTHREE_OBJECT (shiny)), &shiny_velocity, &shiny_pos);

  graphene_vec3_subtract (&shiny_pos,
                          gthree_object_get_position (GTHREE_OBJECT (fish)),
                          &dp);
  graphene_vec3_normalize (&dp, &dp);
  graphene_vec3_scale (&dp, FISH_SPEED, &dp);

  graphene_vec3_add (gthree_object_get_position (GTHREE_OBJECT (fish)),
                     &dp, &fish_pos);

  gthree_object_set_position (GTHREE_OBJECT (shiny), &shiny_pos);

  gthree_object_set_position (GTHREE_OBJECT (fish), &fish_pos);
  gthree_object_look_at (GTHREE_OBJECT (fish), &shiny_pos);


  if (graphene_vec3_get_x (&shiny_pos) > AQUARIUM_SIZE_X / 2 ||
      graphene_vec3_get_x (&shiny_pos) < -AQUARIUM_SIZE_X / 2)
    mx = -1;

  if (graphene_vec3_get_y (&shiny_pos) > AQUARIUM_SIZE_Y / 2 ||
      graphene_vec3_get_y (&shiny_pos) < -AQUARIUM_SIZE_Y / 2)
    my = -1;

  if (graphene_vec3_get_z (&shiny_pos) > AQUARIUM_SIZE_Y / 2 ||
      graphene_vec3_get_z (&shiny_pos) < -AQUARIUM_SIZE_Y / 2)
    mz = -1;

  graphene_vec3_init (&mv, mx, my, mz);
  graphene_vec3_multiply (&shiny_velocity, &mv, &shiny_velocity);

  gtk_widget_queue_draw (widget);

  return G_SOURCE_CONTINUE;
}

static void
init_random_normal (graphene_vec3_t *v, float magnitude)
{
 graphene_vec3_init (v,
                     g_random_double_range (-1, 1),
                     g_random_double_range (-1, 1),
                     g_random_double_range (-1, 1));
  graphene_vec3_normalize (v, v);
  graphene_vec3_scale (v, magnitude, v);
}

static void
screen_changed (GtkWidget *window, GdkScreen *old_screen, gpointer userdata)
{
  GdkScreen *screen = gtk_widget_get_screen (window);
  GdkVisual *visual = gdk_screen_get_rgba_visual (screen);

  if (!visual)
    visual = gdk_screen_get_system_visual (screen);

  gtk_widget_set_visual (window, visual);

  gtk_window_resize (GTK_WINDOW (window),
                     gdk_screen_get_width (screen),
                     gdk_screen_get_height (screen));
}

static gboolean
window_draw (GtkWidget *widget,
             cairo_t   *cr)
{
  GdkScreen *screen = gtk_widget_get_screen (widget);
  GtkAllocation allocation;
  static gboolean did_input_shape = FALSE;

  /* For some reason this doesn't work in realize so we need to delay it here */
  if (!did_input_shape)
    {
      cairo_region_t *region;

      region = cairo_region_create ();
      gtk_widget_input_shape_combine_region (widget, region);
      cairo_region_destroy (region);

      did_input_shape = TRUE;
    }

  gtk_widget_get_allocation (widget, &allocation);
  cairo_translate (cr, allocation.x, allocation.y);
  cairo_rectangle (cr, 0, 0, allocation.width, allocation.height);
  cairo_clip (cr);

  if (gdk_screen_get_rgba_visual (screen) &&  gdk_screen_is_composited (screen))
    cairo_set_source_rgba (cr, 1.0, 1.0, 1.0, 0.0); /* transparent */
  else
    cairo_set_source_rgb (cr, 1.0, 1.0, 1.0); /* opaque white */

  cairo_set_operator (cr, CAIRO_OPERATOR_SOURCE);
  cairo_paint (cr);

  return FALSE;
}

int
main (int argc, char *argv[])
{
  GtkWidget *window, *area;
  GthreeScene *scene;
  GthreePerspectiveCamera *camera;

  gtk_init (&argc, &argv);

  window = gtk_window_new (GTK_WINDOW_POPUP);

  gtk_widget_set_app_paintable (window, TRUE);
  g_signal_connect (window, "screen-changed", G_CALLBACK (screen_changed), NULL);
  screen_changed (window, NULL, NULL);
  g_signal_connect (window, "draw", G_CALLBACK (window_draw), NULL);
  gtk_window_set_decorated (GTK_WINDOW (window), FALSE);

  gtk_window_set_title (GTK_WINDOW (window), "Wanda3");
  gtk_window_set_default_size (GTK_WINDOW (window), 800, 600);
  g_signal_connect (window, "destroy", G_CALLBACK (gtk_main_quit), NULL);

  scene = gthree_scene_new ();
  gthree_scene_set_background_alpha (scene, 0.0);
  mixer = gthree_animation_mixer_new (GTHREE_OBJECT (scene));
  init_scene (scene);

  init_random_normal (&shiny_velocity, SHINY_SPEED);

  camera = gthree_perspective_camera_new (70, 1, 1, 2000);
  gthree_object_set_position_xyz (GTHREE_OBJECT (camera), 0, 0, 500);
  gthree_object_add_child (GTHREE_OBJECT (scene), GTHREE_OBJECT (camera));

  area = gthree_area_new (scene, GTHREE_CAMERA (camera));
  gtk_gl_area_set_has_alpha (GTK_GL_AREA (area), TRUE);
  g_signal_connect (area, "resize", G_CALLBACK (resize_area), camera);
  gtk_widget_set_hexpand (area, TRUE);
  gtk_widget_set_vexpand (area, TRUE);
  gtk_container_add (GTK_CONTAINER (window), area);

  gtk_widget_show (area);

  gtk_widget_add_tick_callback (GTK_WIDGET (area), tick, area, NULL);

  gtk_widget_show (window);

  gtk_main ();

  return EXIT_SUCCESS;
}
